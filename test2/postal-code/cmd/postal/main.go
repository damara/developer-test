package main

import (
	"fmt"

	"postal-code/config/postgresql"

	"net/http"
	"os"

	postalHandler "postal-code/pkg/postal-code/handler"
	postalRepo "postal-code/pkg/postal-code/repository"
	postalUsecase "postal-code/pkg/postal-code/usecase"

	echoPrometheus "github.com/globocom/echo-prometheus"

	config "github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog/log"
)

func main() {

	// if os.Getenv("GO_ENV") == "local" {
	if err := config.Load(".env"); err != nil {
		fmt.Println(".env is not loaded properly")
		fmt.Println(err)
		os.Exit(2)
	}
	// }

	dbConn := postgresql.CreateConnection()

	r := echo.New()
	r.Debug = true
	r.Use(echoPrometheus.MetricsMiddleware())
	r.GET("/metrics", echo.WrapHandler(promhttp.Handler()))
	r.Use(middleware.Recover())
	r.Use(middleware.Logger())
	r.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     []string{"X-Requested-With", "Content-Type", "Authorization"},
		AllowCredentials: true,
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete, http.MethodOptions},
	}))

	api := r.Group("/postal")

	postalRepo := postalRepo.NewPostalCodeRepo(dbConn)
	postalUsecase := postalUsecase.NewPostalCodeRepo(postalRepo)
	postalHandler.NewHTTPHandler(postalUsecase).Mount(api, dbConn)

	err := r.Start(":" + os.Getenv("PORT"))
	if err != nil {
		log.Error().Msg(err.Error())
	}
}
