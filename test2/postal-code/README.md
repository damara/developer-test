# how to install
1. restore the file postalcode.sql to the PostgresSQL database 

2. Make a copy of.env.example and rename it to.env. 

3. In the file .env, set up your PostgresSQL connection. 
    DB_HOST=127.0.0.1
    DB_PORT={{your_port}}
    DB_DATABASE={{your_db_name}}
    DB_USERNAME={{your_username}}
    DB_PASSWORD={{your_password}}

# how to run
1. GO TO THE ROOT FOLDER AND RUN "php artisan key:generate" 
2. GO TO THE ROOT FOLDER AND RUN "php artisan serve" or "php artisan serve --port={{port}}"
    example: php artisan serve or php artisan serve --port=5018

# how to consume
1. Open Postman and import the Postal-code.postman collection.json file. 
2. API endpoint url
    {{hostname}}:{{your_port}}/api/postal/postalcode method POST to get postal code with the postal code
    {{hostname}}:{{your_port}}/api/postal/province method POST to get postal code with the province
    {{hostname}}:{{your_port}}/api/postal/city method POST to get postal code with the city
    {{hostname}}:{{your_port}}/api/postal/district method POST to get postal code with the district
3. if check the log request and response in path /logs/laravel.log
