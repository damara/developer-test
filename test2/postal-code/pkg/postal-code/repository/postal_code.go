package repository

import (
	"postal-code/config/postgresql"
	"postal-code/libs/models"
	"postal-code/pkg/postal-code/model"
)

type postalCodeRepo struct {
	dbConn *postgresql.DbConnection
}

func NewPostalCodeRepo(dbConn *postgresql.DbConnection) PostalCodeRepo {
	return &postalCodeRepo{dbConn: dbConn}
}

func (r *postalCodeRepo) GetProvince(param model.GetProvince) <-chan models.Result {
	p := param
	output := make(chan models.Result)
	var finalResult []model.FinalResult
	go func() {
		defer close(output)
		q := r.dbConn.Db
		sql := `select 
		dpcd.postal_code,
		dpcd.city,
		dpcd.sub_district as district,
		dpcd.urban,
		dpd.province_name as province
		from db_postal_code_data dpcd 
		join db_province_data dpd on dpd.province_code = dpcd.province_code
		where dpd.province_name like upper('%` + p.Province + `%') `
		if err := q.Raw(sql).Scan(&finalResult).Error; err != nil {
			output <- models.Result{Data: err}
		}

		response := &models.Response{Code: 200, MessageCode: 0, Message: "Success", Data: finalResult}
		output <- models.Result{Data: response}
	}()
	return output
}

func (r *postalCodeRepo) GetCity(param model.GetCity) <-chan models.Result {
	p := param
	output := make(chan models.Result)
	var finalResult []model.FinalResult
	go func() {
		defer close(output)
		q := r.dbConn.Db
		sql := `select 
		dpcd.postal_code,
		dpcd.city,
		dpcd.sub_district as district,
		dpcd.urban,
		dpd.province_name as province
		from db_postal_code_data dpcd 
		join db_province_data dpd on dpd.province_code = dpcd.province_code
		where dpcd.city like upper('%` + p.City + `%') `
		if err := q.Raw(sql).Scan(&finalResult).Error; err != nil {
			output <- models.Result{Data: err}
		}

		response := &models.Response{Code: 200, MessageCode: 0, Message: "Success", Data: finalResult}
		output <- models.Result{Data: response}

	}()
	return output
}

func (r *postalCodeRepo) GetState(param model.GetState) <-chan models.Result {
	p := param
	output := make(chan models.Result)
	var finalResult []model.FinalResult
	go func() {
		defer close(output)
		q := r.dbConn.Db
		sql := `select 
		dpcd.postal_code,
		dpcd.city,
		dpcd.sub_district as district,
		dpcd.urban,
		dpd.province_name as province
		from db_postal_code_data dpcd 
		join db_province_data dpd on dpd.province_code = dpcd.province_code
		where dpcd.sub_district like upper('%` + p.District + `%') `

		if err := q.Raw(sql).Scan(&finalResult).Error; err != nil {
			output <- models.Result{Data: err}
		}

		response := &models.Response{Code: 400, MessageCode: 0, Message: "Success", Data: finalResult}
		output <- models.Result{Data: response}
	}()
	return output
}

func (r *postalCodeRepo) GetCode(param model.GetCode) <-chan models.Result {
	p := param
	output := make(chan models.Result)
	var finalResult []model.FinalResult
	go func() {
		defer close(output)
		q := r.dbConn.Db
		sql := `select 
		dpcd.postal_code,
		dpcd.city,
		dpcd.sub_district as district,
		dpcd.urban,
		dpd.province_name as province
		from db_postal_code_data dpcd 
		join db_province_data dpd on dpd.province_code = dpcd.province_code
		where dpcd.postal_code like '%` + p.PostalCode + `%' `

		if err := q.Raw(sql).Scan(&finalResult).Error; err != nil {
			output <- models.Result{Data: err}
		}

		response := &models.Response{Code: 400, MessageCode: 0, Message: "Success", Data: finalResult}
		output <- models.Result{Data: response}
	}()
	return output
}
