package repository

import (
	"postal-code/libs/models"
	"postal-code/pkg/postal-code/model"
)

type PostalCodeRepo interface {
	GetState(model.GetState) <-chan models.Result
	GetCity(model.GetCity) <-chan models.Result
	GetProvince(param model.GetProvince) <-chan models.Result
	GetCode(param model.GetCode) <-chan models.Result
}
