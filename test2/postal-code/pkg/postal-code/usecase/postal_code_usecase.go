package usecase

import (
	"postal-code/libs/models"
	"postal-code/pkg/postal-code/model"
)

type PostalCodeUsecase interface {
	GetState(param model.GetState) <-chan models.Result
	GetCity(param model.GetCity) <-chan models.Result
	GetProvince(param model.GetProvince) <-chan models.Result
	GetCode(param model.GetCode) <-chan models.Result
}
