package usecase

import (
	"postal-code/libs/models"
	"postal-code/pkg/postal-code/model"
	"postal-code/pkg/postal-code/repository"
)

type postalCodeUsecase struct {
	repo repository.PostalCodeRepo
}

func NewPostalCodeRepo(repo repository.PostalCodeRepo) PostalCodeUsecase {
	return &postalCodeUsecase{repo: repo}
}

func (u *postalCodeUsecase) GetState(param model.GetState) <-chan models.Result {
	output := make(chan models.Result)

	go func() {
		defer close(output)

		resp := <-u.repo.GetState(param)

		if resp.Error != nil {
			output <- models.Result{Error: resp.Error}
			return
		}

		output <- models.Result{Data: resp.Data, Error: nil}
	}()

	return output
}

func (u *postalCodeUsecase) GetCity(param model.GetCity) <-chan models.Result {
	output := make(chan models.Result)

	go func() {
		defer close(output)

		resp := <-u.repo.GetCity(param)

		if resp.Error != nil {
			output <- models.Result{Error: resp.Error}
			return
		}

		output <- models.Result{Data: resp.Data, Error: nil}
	}()

	return output
}

func (u *postalCodeUsecase) GetProvince(param model.GetProvince) <-chan models.Result {
	output := make(chan models.Result)

	go func() {
		defer close(output)

		resp := <-u.repo.GetProvince(param)

		if resp.Error != nil {
			output <- models.Result{Error: resp.Error}
			return
		}

		output <- models.Result{Data: resp.Data, Error: nil}
	}()

	return output
}

func (u *postalCodeUsecase) GetCode(param model.GetCode) <-chan models.Result {
	output := make(chan models.Result)

	go func() {
		defer close(output)

		resp := <-u.repo.GetCode(param)

		if resp.Error != nil {
			output <- models.Result{Error: resp.Error}
			return
		}

		output <- models.Result{Data: resp.Data, Error: nil}
	}()

	return output
}
