package model

type GetState struct {
	District string `json:"district"`
}

type GetCity struct {
	City string `json:"city"`
}

type GetProvince struct {
	Province string `json:"province"`
}

type GetCode struct {
	PostalCode string `json:"postal_code"`
}

type FinalResult struct {
	City       string `json:"city"`
	District   string `json:"district"`
	Urban      string `json:"urban"`
	Province   string `json:"province"`
	PostalCode string `json:"postal_code"`
}
