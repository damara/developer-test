package handler

import (
	"postal-code/config/postgresql"
	"postal-code/pkg/postal-code/usecase"

	"github.com/labstack/echo/v4"
)

type HTTPHandler struct {
	usecase usecase.PostalCodeUsecase
}

func NewHTTPHandler(usecase usecase.PostalCodeUsecase) *HTTPHandler {
	return &HTTPHandler{usecase: usecase}
}

func (h *HTTPHandler) Mount(g *echo.Group, dbConn *postgresql.DbConnection) {
	g.POST("/district", h.district)
	g.POST("/city", h.city)
	g.POST("/province", h.province)
	g.POST("/postalcode", h.code)
}
