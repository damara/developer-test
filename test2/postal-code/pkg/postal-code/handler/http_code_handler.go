package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"postal-code/libs/models"
	"postal-code/pkg/postal-code/model"

	"github.com/labstack/echo/v4"
)

func (h *HTTPHandler) district(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	t := model.GetState{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	if t.District == "" {
		return models.ToJSON(c).BadRequest("district cannot be empty")
	}
	param := model.GetState{
		District: t.District,
	}
	result := <-h.usecase.GetState(param)
	if result.Error != nil {
		resp := &models.Response{
			Code:    400,
			Message: result.Error.Error(),
		}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)

}

func (h *HTTPHandler) city(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	t := model.GetCity{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	if t.City == "" {
		return models.ToJSON(c).BadRequest("city cannot be empty")
	}

	param := model.GetCity{
		City: t.City,
	}
	result := <-h.usecase.GetCity(param)
	if result.Error != nil {
		resp := &models.Response{
			Code:    400,
			Message: result.Error.Error(),
		}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)

}
func (h *HTTPHandler) province(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	t := model.GetProvince{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	if t.Province == "" {
		return models.ToJSON(c).BadRequest("province cannot be empty")
	}

	param := model.GetProvince{
		Province: t.Province,
	}
	result := <-h.usecase.GetProvince(param)
	if result.Error != nil {
		resp := &models.Response{
			Code:    400,
			Message: result.Error.Error(),
		}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)

}

func (h *HTTPHandler) code(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	t := model.GetCode{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	if t.PostalCode == "" {
		return models.ToJSON(c).BadRequest("postal_code cannot be empty")
	}

	param := model.GetCode{
		PostalCode: t.PostalCode,
	}
	result := <-h.usecase.GetCode(param)
	if result.Error != nil {
		resp := &models.Response{
			Code:    400,
			Message: result.Error.Error(),
		}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)

}
