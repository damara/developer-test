module postal-code

go 1.14

require (
	github.com/globocom/echo-prometheus v0.1.2
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.4.0
	github.com/prometheus/client_golang v1.11.0
	github.com/rs/zerolog v1.23.0
)
