<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostalCodeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/postal/postalcode',  [PostalCodeController::class, 'postalcode'])->middleware('log.api');
Route::post('/postal/province',  [PostalCodeController::class, 'province'])->middleware('log.api');
Route::post('/postal/city',  [PostalCodeController::class, 'city'])->middleware('log.api');
Route::post('/postal/district',  [PostalCodeController::class, 'district'])->middleware('log.api');
