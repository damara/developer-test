<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;


class PostalCodeController extends Controller
{
    
    public function postalcode(Request $request)
    {

    $postal_code = $request->postal_code;
    
    if (!empty($postal_code)) {
        $results = DB::table('db_postal_code_data')
        ->join('db_province_data', 'db_province_data.province_code', '=', 'db_postal_code_data.province_code')
        ->select('db_postal_code_data.postal_code', 'db_postal_code_data.sub_district as district', 'db_postal_code_data.urban', 'db_province_data.province_name as province')
        ->where('db_postal_code_data.postal_code', 'like', "%".$postal_code."%")
        ->get();

        return response()->json([
            'code' => 200,
            'message_code'   => 0,
            'message'   => 'Success',
            'data'      => $results
        ], 200);
    }else{
        return response()->json([
            'code' => 400,
            'message_code'   => 1,
            'message'   => "postal_code can't be empty",
            'data'      => null
        ], 400);
    }

    }

    public function province(Request $request)
    {
     
    $province      = $request->input('province');

    if ($province != "") {
    $province = strtoupper($province);
        $results = DB::table('db_postal_code_data')
        ->join('db_province_data', 'db_province_data.province_code', '=', 'db_postal_code_data.province_code')
        ->select('db_postal_code_data.postal_code', 'db_postal_code_data.sub_district as district', 'db_postal_code_data.urban', 'db_province_data.province_name as province')
        ->where('db_province_data.province_name', 'like', "%".$province."%")
        ->get();

        return response()->json([
            'code' => 200,
            'message_code'   => 0,
            'message'   => 'Success',
            'data'      => $results
        ], 200);
    }else{
        return response()->json([
            'code' => 400,
            'message_code'   => 1,
            'message'   => "province can't be empty",
            'data'      => null
        ], 400);
    }

    }

    public function city(Request $request)
    {
        $city      = $request->input('city');
     
        if ($city != "") {
            $city = strtoupper($city);
            $results = DB::table('db_postal_code_data')
            ->join('db_province_data', 'db_province_data.province_code', '=', 'db_postal_code_data.province_code')
            ->select('db_postal_code_data.postal_code', 'db_postal_code_data.sub_district as district', 'db_postal_code_data.urban', 'db_province_data.province_name as province')
            ->where('db_postal_code_data.city', 'like', "%".$city."%")
            ->get();
    
            return response()->json([
                'code' => 200,
                'message_code'   => 0,
                'message'   => 'Success',
                'data'      => $results
            ], 200);
        }else{
            return response()->json([
                'code' => 400,
                'message_code'   => 1,
                'message'   => "city can't be empty",
                'data'      => null
            ], 400);
        }

    }

    public function district(Request $request)
    {
     
     $district      = $request->input('district');
     
     if ($district != "") {
         $district = strtoupper($district);
         $results = DB::table('db_postal_code_data')
         ->join('db_province_data', 'db_province_data.province_code', '=', 'db_postal_code_data.province_code')
         ->select('db_postal_code_data.postal_code', 'db_postal_code_data.sub_district as district', 'db_postal_code_data.urban', 'db_province_data.province_name as province')
         ->where('db_postal_code_data.sub_district', 'like', "%".$district."%")
         ->get();
 
         return response()->json([
            'code' => 200,
             'message_code'   => 0,
             'message'   => 'Success',
             'data'      => $results
         ], 200);
     }else{
         return response()->json([
            'code' => 400,
             'message_code'   => 1,
             'message'   => "district can't be empty",
             'data'      => null
         ], 400);
     }

    }
}
