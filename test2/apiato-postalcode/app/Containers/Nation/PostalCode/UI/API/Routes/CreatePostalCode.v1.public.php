<?php

/**
 * @apiGroup           PostalCode
 * @apiName            createPostalCode
 *
 * @api                {POST} /v1/postalcode Create Postal Code
 * @apiDescription     This API to Create Postal Code
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
  * @apiParam           {Int}   province_id
 * @apiParam            {String}  urban
  * @apiParam           {String}   city
 * @apiParam            {String}  postalcode
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": {
        "province": "Yogyakarta",
        "urban": "Mlati",
        "city": "Sleman",
        "postalcode": "55111"
    },
    "meta": {
        "include": [],
        "custom": []
    }
}
 */

use App\Containers\Nation\PostalCode\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('postalcode', [Controller::class, 'createPostalCode'])
    ->name('api_postalcode_create_postal_code');

