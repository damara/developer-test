<?php

/**
 * @apiGroup           PostalCode
 * @apiName            updatePostalCode
 *
 * @api                {PATCH} /v1/postalcode/:id Update Postal Code
 * @apiDescription     This API to Update Postal Code
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
  * @apiParam           {String}   province_id
 * @apiParam            {String}  urban
  * @apiParam           {String}   city
 * @apiParam            {String}  postalcode
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": {
        "province": "Yogyakarta",
        "urban": "Bangunjiwoooo",
        "city": "Bantul",
        "postalcode": "55184"
    },
    "meta": {
        "include": [],
        "custom": []
    }
}
 */

use App\Containers\Nation\PostalCode\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('postalcode/{id}', [Controller::class, 'updatePostalCode'])
    ->name('api_postalcode_update_postal_code');

