<?php

/**
 * @apiGroup           PostalCode
 * @apiName            getAllPostalCodes
 *
 * @api                {GET} /v1/postalcode Get All Postal Codes
 * @apiDescription     this for get all Get All Postal Codes
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": [
        {
            "province": "Yogyakarta",
            "urban": "Bangunjiwoooo",
            "city": "Bantul",
            "postalcode": "55184"
        },
        {
            "province": "Yogyakarta",
            "urban": "Kasihan",
            "city": "Bantul",
            "postalcode": "551843"
        },
        {
            "province": "Yogyakarta",
            "urban": "Donotirto",
            "city": "Bantul",
            "postalcode": "55185"
        }
    ],
    "meta": {
        "include": [],
        "custom": [],
        "pagination": {
            "total": 3,
            "count": 3,
            "per_page": 10,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
        }
    }
}
 */

use App\Containers\Nation\PostalCode\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('postalcode', [Controller::class, 'getAllPostalCodes'])
    ->name('api_postalcode_get_all_postal_codes');

