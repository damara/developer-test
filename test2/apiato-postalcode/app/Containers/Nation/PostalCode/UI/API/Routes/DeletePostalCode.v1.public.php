<?php

/**
 * @apiGroup           PostalCode
 * @apiName            deletePostalCode
 *
 * @api                {DELETE} /v1/postalcode/:id Delete Postal Code
 * @apiDescription     This API to Delete Postal Code
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  No response
}
 */

use App\Containers\Nation\PostalCode\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('postalcode/{id}', [Controller::class, 'deletePostalCode'])
    ->name('api_postalcode_delete_postal_code');

