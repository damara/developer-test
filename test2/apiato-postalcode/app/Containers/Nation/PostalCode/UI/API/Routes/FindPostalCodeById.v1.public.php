<?php

/**
 * @apiGroup           PostalCode
 * @apiName            findPostalCodeById
 *
 * @api                {GET} /v1/postalcode/:id Find Postal Code By Id
 * @apiDescription     This API to Find Postal Code By Id
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": {
        "province": "Yogyakarta",
        "urban": "Kasihan",
        "city": "Bantul",
        "postalcode": "551843"
    },
    "meta": {
        "include": [],
        "custom": []
    }
}
 */

use App\Containers\Nation\PostalCode\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('postalcode/{id}', [Controller::class, 'findPostalCodeById'])
    ->name('api_postalcode_find_postal_code_by_id');

