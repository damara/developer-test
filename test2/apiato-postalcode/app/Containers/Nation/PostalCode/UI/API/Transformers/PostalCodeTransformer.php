<?php

namespace App\Containers\Nation\PostalCode\UI\API\Transformers;

use App\Containers\Nation\Province\Models\Province;
use App\Containers\Nation\PostalCode\Models\PostalCode;
use App\Ship\Parents\Transformers\Transformer;

class PostalCodeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(PostalCode $postalcode): array
    {
        $data = Province::find($postalcode->province_id);
        $response = [
            "province" => $data->province_name,
            "urban" => $postalcode->urban,
            "city" => $postalcode->city,
            "postalcode" => $postalcode->postalcode,  
        ];

        return $response;
    }
}
