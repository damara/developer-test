<?php

namespace App\Containers\Nation\PostalCode\UI\API\Controllers;

use App\Containers\Nation\PostalCode\UI\API\Requests\CreatePostalCodeRequest;
use App\Containers\Nation\PostalCode\UI\API\Requests\DeletePostalCodeRequest;
use App\Containers\Nation\PostalCode\UI\API\Requests\GetAllPostalCodesRequest;
use App\Containers\Nation\PostalCode\UI\API\Requests\FindPostalCodeByIdRequest;
use App\Containers\Nation\PostalCode\UI\API\Requests\UpdatePostalCodeRequest;
use App\Containers\Nation\PostalCode\UI\API\Transformers\PostalCodeTransformer;
use App\Containers\Nation\PostalCode\Actions\CreatePostalCodeAction;
use App\Containers\Nation\PostalCode\Actions\FindPostalCodeByIdAction;
use App\Containers\Nation\PostalCode\Actions\GetAllPostalCodesAction;
use App\Containers\Nation\PostalCode\Actions\UpdatePostalCodeAction;
use App\Containers\Nation\PostalCode\Actions\DeletePostalCodeAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createPostalCode(CreatePostalCodeRequest $request): JsonResponse
    {
        $postalcode = app(CreatePostalCodeAction::class)->run($request);
        return $this->created($this->transform($postalcode, PostalCodeTransformer::class));
    }

    public function findPostalCodeById(FindPostalCodeByIdRequest $request): array
    {
        $postalcode = app(FindPostalCodeByIdAction::class)->run($request);
        return $this->transform($postalcode, PostalCodeTransformer::class);
    }

    public function getAllPostalCodes(GetAllPostalCodesRequest $request): array
    {
        $postalcodes = app(GetAllPostalCodesAction::class)->run($request);
        return $this->transform($postalcodes, PostalCodeTransformer::class);
    }

    public function updatePostalCode(UpdatePostalCodeRequest $request): array
    {
        $postalcode = app(UpdatePostalCodeAction::class)->run($request);
        return $this->transform($postalcode, PostalCodeTransformer::class);
    }

    public function deletePostalCode(DeletePostalCodeRequest $request): JsonResponse
    {
        app(DeletePostalCodeAction::class)->run($request);
        return $this->noContent();
    }
}
