<?php

namespace App\Containers\Nation\PostalCode\Models;

use App\Ship\Parents\Models\Model;

class PostalCode extends Model
{
    protected $table = 'postal_codes';
    protected $fillable = [
        'province_id',
        'urban',
        'city',
        'postalcode',

    ];

    
    public function province() {
        return $this->belongsTo('App\Containers\Nation\Province\Models');
    }

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'PostalCode';
}
