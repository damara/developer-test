<?php

namespace App\Containers\Nation\PostalCode\Actions;

use App\Containers\Nation\PostalCode\Models\PostalCode;
use App\Containers\Nation\PostalCode\Tasks\FindPostalCodeByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindPostalCodeByIdAction extends Action
{
    public function run(Request $request): PostalCode
    {
        return app(FindPostalCodeByIdTask::class)->run($request->id);
    }
}
