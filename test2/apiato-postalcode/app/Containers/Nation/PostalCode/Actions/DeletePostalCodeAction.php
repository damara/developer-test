<?php

namespace App\Containers\Nation\PostalCode\Actions;

use App\Containers\Nation\PostalCode\Tasks\DeletePostalCodeTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeletePostalCodeAction extends Action
{
    public function run(Request $request)
    {
        return app(DeletePostalCodeTask::class)->run($request->id);
    }
}
