<?php

namespace App\Containers\Nation\PostalCode\Actions;

use App\Containers\Nation\PostalCode\Models\PostalCode;
use App\Containers\Nation\PostalCode\Tasks\CreatePostalCodeTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreatePostalCodeAction extends Action
{
    public function run(Request $request): PostalCode
    {
        $data = $request->sanitizeInput([
            // add your request data here
            "province_id", $request->province_id,
            "urban" => $request->urban,
            "city" => $request->city,
            "postalcode" => $request->postalcode,  
        ]);

        return app(CreatePostalCodeTask::class)->run($data);
    }
}
