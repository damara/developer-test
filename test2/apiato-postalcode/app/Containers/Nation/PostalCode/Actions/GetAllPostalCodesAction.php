<?php

namespace App\Containers\Nation\PostalCode\Actions;

use App\Containers\Nation\PostalCode\Tasks\GetAllPostalCodesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllPostalCodesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllPostalCodesTask::class)->addRequestCriteria()->run();
    }
}
