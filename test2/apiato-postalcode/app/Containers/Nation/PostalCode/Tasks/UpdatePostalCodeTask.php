<?php

namespace App\Containers\Nation\PostalCode\Tasks;

use App\Containers\Nation\PostalCode\Data\Repositories\PostalCodeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdatePostalCodeTask extends Task
{
    protected PostalCodeRepository $repository;

    public function __construct(PostalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
