<?php

namespace App\Containers\Nation\PostalCode\Tasks;

use App\Containers\Nation\PostalCode\Data\Repositories\PostalCodeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllPostalCodesTask extends Task
{
    protected PostalCodeRepository $repository;

    public function __construct(PostalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
