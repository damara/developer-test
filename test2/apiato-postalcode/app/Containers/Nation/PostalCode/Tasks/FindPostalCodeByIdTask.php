<?php

namespace App\Containers\Nation\PostalCode\Tasks;

use App\Containers\Nation\PostalCode\Data\Repositories\PostalCodeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindPostalCodeByIdTask extends Task
{
    protected PostalCodeRepository $repository;

    public function __construct(PostalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
