<?php

namespace App\Containers\Nation\PostalCode\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class PostalCodeRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        "urban" => "like",
        "city" => "like",
        "postalcode" => "like",        
        // ...
    ];
}
