<?php

namespace App\Containers\Nation\Province\UI\API\Transformers;

use App\Containers\Nation\Province\Models\Province;
use App\Ship\Parents\Transformers\Transformer;

class ProvinceTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Province $province): array
    {
        $response = [
            'province_name' => $province->province_name,
            'province_code' => $province->province_code,

        ];

        return $response;
    }
}
