<?php

namespace App\Containers\Nation\Province\UI\API\Controllers;

use App\Containers\Nation\Province\UI\API\Requests\CreateProvinceRequest;
use App\Containers\Nation\Province\UI\API\Requests\DeleteProvinceRequest;
use App\Containers\Nation\Province\UI\API\Requests\GetAllProvincesRequest;
use App\Containers\Nation\Province\UI\API\Requests\FindProvinceByIdRequest;
use App\Containers\Nation\Province\UI\API\Requests\UpdateProvinceRequest;
use App\Containers\Nation\Province\UI\API\Transformers\ProvinceTransformer;
use App\Containers\Nation\Province\Actions\CreateProvinceAction;
use App\Containers\Nation\Province\Actions\FindProvinceByIdAction;
use App\Containers\Nation\Province\Actions\GetAllProvincesAction;
use App\Containers\Nation\Province\Actions\UpdateProvinceAction;
use App\Containers\Nation\Province\Actions\DeleteProvinceAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createProvince(CreateProvinceRequest $request): JsonResponse
    {
        $province = app(CreateProvinceAction::class)->run($request);
        return $this->created($this->transform($province, ProvinceTransformer::class));
    }

    public function findProvinceById(FindProvinceByIdRequest $request): array
    {
        $province = app(FindProvinceByIdAction::class)->run($request);
        return $this->transform($province, ProvinceTransformer::class);
    }

    public function getAllProvinces(GetAllProvincesRequest $request): array
    {
        $provinces = app(GetAllProvincesAction::class)->run($request);
        return $this->transform($provinces, ProvinceTransformer::class);
    }

    public function updateProvince(UpdateProvinceRequest $request): array
    {
        $province = app(UpdateProvinceAction::class)->run($request);
        return $this->transform($province, ProvinceTransformer::class);
    }

    public function deleteProvince(DeleteProvinceRequest $request): JsonResponse
    {
        app(DeleteProvinceAction::class)->run($request);
        return $this->noContent();
    }
}
