<?php

/**
 * @apiGroup           Province
 * @apiName            createProvince
 *
 * @api                {POST} /v1/province Create Province
 * @apiDescription     This Endpoint to Create data Province
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  [province_name]
 * @apiParam           {String}  [province_code]
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": {
        "province_name": "Jakarta",
        "province_code": "Jak"
    },
    "meta": {
        "include": [],
        "custom": []
    }
}
 */

use App\Containers\Nation\Province\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('province', [Controller::class, 'createProvince'])
    ->name('api_province_create_province');

