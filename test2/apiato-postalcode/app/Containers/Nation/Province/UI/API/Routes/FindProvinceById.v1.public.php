<?php

/**
 * @apiGroup           Province
 * @apiName            findProvinceById
 *
 * @api                {GET} /v1/province/:id Find Province By Id
 * @apiDescription     This Endpoint to Find Province By Id
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": {
        "province_name": "Yogyakarta",
        "province_code": "Yog"
    },
    "meta": {
        "include": [],
        "custom": []
    }
}
 */

use App\Containers\Nation\Province\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('province/{id}', [Controller::class, 'findProvinceById'])
    ->name('api_province_find_province_by_id');

