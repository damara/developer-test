<?php

/**
 * @apiGroup           Province
 * @apiName            deleteProvince
 *
 * @api                {DELETE} /v1/province/:id Delete Province.
 * @apiDescription     This Endpoint to Delete data Province
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  No response
}
 */

use App\Containers\Nation\Province\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('province/{id}', [Controller::class, 'deleteProvince'])
    ->name('api_province_delete_province');

