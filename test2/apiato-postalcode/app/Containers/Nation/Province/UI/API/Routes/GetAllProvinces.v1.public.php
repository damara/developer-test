<?php

/**
 * @apiGroup           Province
 * @apiName            getAllProvinces
 *
 * @api                {GET} /v1/province Get All Province
 * @apiDescription     This Endpoint to get all data
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": [
        {
            "province_name": "Jawa Tengah",
            "province_code": "Jateng"
        },
        {
            "province_name": "Yogyakarta",
            "province_code": "Yog"
        }
    ],
    "meta": {
        "include": [],
        "custom": [],
        "pagination": {
            "total": 3,
            "count": 3,
            "per_page": 10,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
        }
    }
}
 */

use App\Containers\Nation\Province\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('province', [Controller::class, 'getAllProvinces'])
    ->name('api_province_get_all_provinces');

