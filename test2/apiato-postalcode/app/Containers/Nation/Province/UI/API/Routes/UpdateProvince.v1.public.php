<?php

/**
 * @apiGroup           Province
 * @apiName            updateProvince
 *
 * @api                {PATCH} /v1/province/:id Update Province
 * @apiDescription     This API to Update Province
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  [province_name]
 * @apiParam           {String}  [province_code]
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
    "data": {
        "province_name": "Jakarta",
        "province_code": "Jak"
    },
    "meta": {
        "include": [],
        "custom": []
    }
}
 */

use App\Containers\Nation\Province\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('province/{id}', [Controller::class, 'updateProvince'])
    ->name('api_province_update_province');

