<?php

namespace App\Containers\Nation\Province\Actions;

use App\Containers\Nation\Province\Models\Province;
use App\Containers\Nation\Province\Tasks\UpdateProvinceTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateProvinceAction extends Action
{
    public function run(Request $request): Province
    {
        $data = $request->sanitizeInput([
            // add your request data here
            "province_name" => $request->province_name,
            "province_code" => $request->province_code,
        ]);

        return app(UpdateProvinceTask::class)->run($request->id, $data);
    }
}
