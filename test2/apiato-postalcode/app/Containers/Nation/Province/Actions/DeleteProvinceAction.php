<?php

namespace App\Containers\Nation\Province\Actions;

use App\Containers\Nation\Province\Tasks\DeleteProvinceTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteProvinceAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteProvinceTask::class)->run($request->id);
    }
}
