<?php

namespace App\Containers\Nation\Province\Actions;

use App\Containers\Nation\Province\Models\Province;
use App\Containers\Nation\Province\Tasks\FindProvinceByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindProvinceByIdAction extends Action
{
    public function run(Request $request): Province
    {
        return app(FindProvinceByIdTask::class)->run($request->id);
    }
}
