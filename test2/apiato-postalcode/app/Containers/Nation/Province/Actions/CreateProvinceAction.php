<?php

namespace App\Containers\Nation\Province\Actions;

use App\Containers\Nation\Province\Models\Province;
use App\Containers\Nation\Province\Tasks\CreateProvinceTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateProvinceAction extends Action
{
    public function run(Request $request): Province
    {
        
        $data = $request->sanitizeInput([
            // add your request data here
            "province_name" => $request->province_name,
            "province_code" => $request->province_code,
        ]);

        return app(CreateProvinceTask::class)->run($data);
    }
}
