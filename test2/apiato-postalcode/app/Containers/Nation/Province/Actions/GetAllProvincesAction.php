<?php

namespace App\Containers\Nation\Province\Actions;

use App\Containers\Nation\Province\Tasks\GetAllProvincesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllProvincesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllProvincesTask::class)->addRequestCriteria()->run();
    }
}
