<?php

namespace App\Containers\Nation\Province\Models;

use App\Ship\Parents\Models\Model;

class Province extends Model
{
    protected $table = 'provinces';
    protected $fillable = [
        'id',
        'province_name',
        'province_code',

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function province() {
        return $this->hasMany('App\Containers\Nation\PostalCode\Models');
    }

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Province';
}
