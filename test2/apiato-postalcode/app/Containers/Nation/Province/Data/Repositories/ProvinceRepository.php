<?php

namespace App\Containers\Nation\Province\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class ProvinceRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        'province_name' => 'like',
        'province_code' => '=',
        'created_at' => 'like',
        // ...
    ];
}
