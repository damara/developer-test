<?php

namespace App\Containers\Nation\Province\Data\Seeders;

use App\Containers\Nation\Province\Tasks\CreateProvinceTask;
use App\Ship\Parents\Seeders\Seeder;

class ProvinceSeeder_1 extends Seeder
{
    public function run(): void
    {
        $cars = array('DKI-Jakarta', 'Jak');
        app(CreateProvinceTask::class)->run($cars);
        // app(CreateProvinceTask::class)->run('Jawa Barat', 'Jabar');
        // app(CreateProvinceTask::class)->run('Jawa Tengah', 'Jateng');
        // app(CreateProvinceTask::class)->run('DI Yogyakarta', 'Yog');
    }
}
