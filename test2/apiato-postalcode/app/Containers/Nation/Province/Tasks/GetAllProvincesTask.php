<?php

namespace App\Containers\Nation\Province\Tasks;

use App\Containers\Nation\Province\Data\Repositories\ProvinceRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllProvincesTask extends Task
{
    protected ProvinceRepository $repository;

    public function __construct(ProvinceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
