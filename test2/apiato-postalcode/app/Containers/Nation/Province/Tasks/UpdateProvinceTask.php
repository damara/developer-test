<?php

namespace App\Containers\Nation\Province\Tasks;

use App\Containers\Nation\Province\Data\Repositories\ProvinceRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateProvinceTask extends Task
{
    protected ProvinceRepository $repository;

    public function __construct(ProvinceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
