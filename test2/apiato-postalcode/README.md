# how to install
1. create Database in the MySQL database 
2. Make a copy of.env.example and rename it to.env. 
3. In the file .env, set up your MySQL connection. 
    DB_HOST=127.0.0.1
    DB_PORT={{your_port}}
    DB_DATABASE={{your_db_name}}
    DB_USERNAME={{your_username}}
    DB_PASSWORD={{your_password}}

    # set your hostname API url and web app url
    APP_URL=localhost
    API_URL=localhost

4. php artisan migrate
5. php artisan db:seed
6. npm install
7. php artisan apiato:apidoc

# how to run
1. GO TO THE ROOT FOLDER AND RUN "php artisan serve" or "php artisan serve --port={{port}}"
    example: php artisan serve or php artisan serve --port=5018

# how to consume
1. Open Postman and import the apiato-laravel.postman_collection.json file. 
2. documentation check the apiato-laravel.postman_collection.json file or check in the {{your running program}}/docs/
