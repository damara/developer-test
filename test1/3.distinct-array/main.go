package main

import "fmt"

func main() {
	row := []int{1, 2, 3}
	fmt.Println(distinctArray(row))
}

func distinctArray(arr []int) [][]int {
	array1 := [][]int{}

	for i := 0; i < len(arr); i++ {
		object_1 := arr[i]
		for j := i; j < len(arr); j++ {
			array2 := append([]int{object_1}, arr[j])
			array1 = append(array1, array2)
		}
	}

	return array1
}
