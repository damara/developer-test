package main

import (
	"fmt"
	"sort"
)

func main() {
	give_array := []int{0, 1, 2, 2, 1, 0, 0, 2, 0, 1, 1, 0}
	sort.Ints(give_array)
	fmt.Println("before sorting", give_array)
	fmt.Println("after sorting", give_array)
}
