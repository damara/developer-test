package main

import (
	"fmt"
)

func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

func maxProduct(arr []int, n int) {
	var result int
	res := []int{}

	for i := 0; i < n; i++ {
		object_array := arr[i]
		for j := i + 1; j < n; j++ {
			result = Max(result, object_array)
			res = append(res, arr[j])
			object_array *= arr[j]
		}
		result = Max(result, object_array)
	}
	fmt.Println("the maximum product sub-array is ", res, "having product", result)
}

func main() {
	give_array := []int{-6, 4, -5, 8, -10, 0, 8}
	n := len(give_array)
	maxProduct(give_array, n)
}
