package main

import (
	"fmt"
)

func CheckArrayDuplicate(array []int) int {
	checked := make(map[int]bool)
	var list int

	for _, object_array := range array {

		if _, value := checked[object_array]; !value {
			checked[object_array] = true
		} else {
			list = object_array
		}
	}
	return list
}

func main() {
	give_array := []int{1, 2, 3, 4, 2}
	fmt.Println(give_array)
	fmt.Println("the duplicate element is ", CheckArrayDuplicate(give_array))
}
